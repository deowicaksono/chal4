    //testing jscript tanpa class
    
    
    function computerRand(){
        const compNum = Math.floor(Math.random()*3);
        return compNum;
    }
    function compChoose(){
        let temp = computerRand();
        console.log(temp);
        if(temp == 0){
            return "batu";
        }
        else if(temp == 1){
            return "kertas";
        }
        else{
            return "gunting";
        }
    }

    function disableButton(){
        let tempobtn = document.getElementsByName("playerbtn");
        for( let i = 0; i< tempobtn.length ; i++){

        
        tempobtn[i].disabled=true;
        }
    
    }
    function highlight(playPick,compPick){
        let tempobtn = document.getElementsByName("playerimg");
        if(playPick=="batu"){tempobtn[0].style="background-color:white"}
        else if(playPick=="kertas"){tempobtn[1].style="background-color:white"}
        else if(playPick=="gunting"){tempobtn[2].style="background-color:white"}

        let compbtn = document.getElementsByName("compbtn");
        if(compPick=="batu"){compbtn[0].style="background-color:white"}
        else if(compPick=="kertas"){compbtn[1].style="background-color:white"}
        else if(compPick=="gunting"){compbtn[2].style="background-color:white"}
    }
    
    function winner(playerChoose){
        let tempPlayer = playerChoose;
        let tempComp = compChoose();
        let stats= document.getElementById("status")
        disableButton();
        highlight(tempPlayer,tempComp)

        console.log(`pilihan player: ${tempPlayer}`);
        console.log(`pilihan Computer: ${tempComp}`);
        //kondisi draw
        if(tempPlayer==tempComp){
            stats.innerText="DRAW!";
        }
        //kondisi player pilih batu
        if(tempPlayer=="batu"){
            if(tempComp=="kertas")  {   stats.innerText="Computer Win!";}
            if(tempComp=="gunting") {   stats.innerText="Player Win!";}
        }
        if(tempPlayer=="kertas"){
            if(tempComp=="gunting") {   stats.innerText="Computer Win!";}
            if(tempComp=="batu")    {   stats.innerText="Player Win!";}
        }
        if(tempPlayer=="gunting"){
            if(tempComp=="batu")    {   stats.innerText="Computer Win!";}
            if(tempComp=="kertas")  {   stats.innerText="Player Win!";}
        }
    }


    function resetButton(){
        let tempobtn = document.getElementsByName("playerbtn");
        let playerImg = document.getElementsByName("playerimg");
        let compbtn = document.getElementsByName("compbtn");
        for( let i = 0; i< tempobtn.length ; i++){

        
        tempobtn[i].disabled=false;
        playerImg[i].style="background-color:transparent;";
        compbtn[i].style="background-color:transparent;";
        }
        let stats= document.getElementById("status");
        stats.innerText="VS";
    }

function playerChoose(temp){
    this.temp=temp;
    winner(this.temp);
}